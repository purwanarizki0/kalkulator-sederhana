#include <iostream>
#include <iomanip>
using namespace std;
int main()
{
    string nama, status;
    char golongan;
    int gajiPokok, tunjangan, gajiBersih;
    float persentase, potonganIuran;
    cout << "nama karyawan : ";
    cin >> nama;
    cout << "Golongan (A/B) : ";
    cin >> golongan;
    cout << "Status (Nikah/Belum) : ";
    cin >> status;
    if (golongan == 'A')
    {
        gajiPokok = 200000;
    }
    if (golongan == 'B')
    {
        gajiPokok = 350000;
    }
    cout << "Gaji pokok : Rp. " << gajiPokok << endl;

    if (golongan == 'A' && status == "Nikah")
    {
        tunjangan = 50000;
    }
    if (golongan == 'A' && status == "Belum")
    {
        tunjangan = 25000;
    }
    if (golongan == 'B' && status == "Nikah")
    {
        tunjangan = 75000;
    }
    if (golongan == 'B' && status == "Belum")
    {
        tunjangan = 60000;
    }
    cout << "Tunjangan : Rp. " << tunjangan << endl;

    if (gajiPokok <= 300000)
    {
        persentase = 0.05;
    }
    if (gajiPokok > 300000)
    {
        persentase = 0.1;
    }
    potonganIuran = (gajiPokok + tunjangan) * persentase;
    cout << "potongan iuran : Rp. " << potonganIuran << endl;
    gajiBersih = gajiPokok + tunjangan - potonganIuran;
    cout << "Gaji Bersih : Rp. " << gajiBersih;
}