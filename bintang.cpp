/*
   Nama Program : bintang.cc
   Tgl buat     : 7 November 2023
   Deskripsi    : mencetak bintang
*/

#include <iostream>
#include <stdlib.h>

using namespace std;

int main()
{
  system("clear");
  int baris;
  cout << "Masukkan jumlah baris: ";
  cin >> baris;
  for (int i=1;i<=baris;i++){
    for (int j=1;j<=i;j++){
      cout<<"*";
    }
    cout<<"\n";
  }

  for (int i=1;i<=baris;i++){
    for (int j=baris-1;j>=i;j--){
      cout<<"*";
    }
    cout<<"\n";
  }
}
