#include <iostream>
#include <iomanip>
using namespace std;
int main (){
    int data, hasil;
    float total, rata;
    int i =1;

    do {

        cout<<"masukan data ke-"<<i<<": ";
        cin>> data;
        total +=data;

        i++;
    }while (data !=0);

    cout<<endl;
    hasil = i-2;
    cout<<"Banyaknya Data \t\t:"<<hasil<<endl;
    cout<<"Total Nilai Data \t:"<<setprecision(2)<<total<<endl;
    rata = total / hasil;
    cout<<"Rata - rata nilai data \t:"<<setprecision(2)<<rata;  
}
