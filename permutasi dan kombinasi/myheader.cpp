#include <iostream>
using namespace std;

int faktorial(int X) {
    int i = 1, f = 1;
    while (i <= X) {
        f = f * i;
        i++;
    }
    return f;
}

void menu() {
    cout << "PERMUTASI DAN KOMBINASI" << endl;
    cout << "=======================" << endl << endl;
}

void proses(char& pilihan, int& bilangan1, int& bilangan2, int& hasil) {
    cout << "Permutasi atau Kombinasi ? (p/k) : ";
    cin >> pilihan;
    cout << endl;
    cout << "Tentukan nilai bilangan pertama : ";
    cin >> bilangan1;
    cout << "Tentukan nilai bilangan kedua : ";
    cin >> bilangan2;
    cout << endl;
    hasil = bilangan1 - bilangan2;
}

void hitungDanTampilkan(char pilihan, int bilangan1, int bilangan2, int hasil) {
    int permutasi, kombinasi;

    switch (pilihan) {
    case 'p':
        permutasi = faktorial(bilangan1) / faktorial(hasil);
        cout << "Hasil permutasi dari " << bilangan1 << " dan " << bilangan2 << " adalah " << permutasi << endl;
        break;

    case 'k':
        kombinasi = faktorial(bilangan1) / (faktorial(hasil) * faktorial(bilangan2));
        cout << "Hasil kombinasi dari " << bilangan1 << " dan " << bilangan2 << " adalah " << kombinasi << endl;
        break;
    default:
        cout << "Pilihan tidak valid." << endl;
    }
}


